/* Welcome to The JavaScript Derby!
Current Version: 0.0.01 (basically brand new)
hi
*/
// requirements
const btn = document.getElementById("play");
const start = document.getElementById("start");
const game = document.getElementById("game");
const chosenHorse = document.getElementById("selection");
const startSound = document.getElementById("startsound");
const instructions = document.getElementById("instructions");
const rmaudio = document.getElementById("rmaudio");
const missPersia = document.getElementById("missPersia");
const theJet = document.getElementById("theJet");
const costco = document.getElementById("costco");
const rusty = document.getElementById("rusty");
const germaine = document.getElementById("germaine");
const winner = document.getElementById("winner");
const sessionWinners = document.getElementById("sessionWinners");
const delay = 500;
const finishLine = document.getElementById("finishLine");
const introAudio = document.getElementById("introaudio");
const gallop = document.getElementById("gallop");
const audience = document.getElementById("audience");
const gameover = document.getElementById("gameover");
const persiaLabel = document.getElementById("persiaLabel");
const rustyLabel = document.getElementById("rustyLabel");
const labelTheJet = document.getElementById("labelTheJet");
const labelGermaine = document.getElementById("labelGermaine");
const labelCostco = document.getElementById("labelCostco");
const winnersText = document.getElementById("winnersText");
const missMargin = window.getComputedStyle(missPersia, null).getPropertyValue("margin-left");
const rustyMargin = window.getComputedStyle(rusty, null).getPropertyValue("margin-left"); 
const costcoMargin = window.getComputedStyle(costco, null).getPropertyValue("margin-left");
const germaineMargin = window.getComputedStyle(germaine, null).getPropertyValue("margin-left");
const theJetMargin = window.getComputedStyle(theJet, null).getPropertyValue("margin-left");
const finishLineMargin = window.getComputedStyle(finishLine, null).getPropertyValue("margin-left");
const holderText = document.getElementById("holderText");
let winnersList = [];

introAudio.volume = 0.05;
gallop.volume = 0.05;
audience.volume = 0.05;
startSound.volume = 0.05;

window.onload = function() {
    introAudio.play();
}
let selection
// horses

instructions.innerHTML = "In order to play the game, please select a horse in the prompt that shows when you click 'Play'. The horses will move along the colored track! The winner is chosen by complete luck. Currently, this is a singleplayer game, with more options to be added soon. TRIGGER WARNING: This game uses an audio sound whenever the game is started that contains the sound of a starter gun firing like they do in real horse races. If this is triggering in any way, please click the 'no audio' button below. Thank you!"

let str = "Welcome to The JavaScript Derby! This game has been worked on in attempt to deliver a fun experience for you, the user. Functionality is limited, but as future versions are developed, so shall be the content. Enjoy!!", 
i = 0,
tag,
text;

function autoType() {
    text = str.slice(0, ++i)
    if (text === str) return;

    document.getElementById("intro").innerHTML = text;

    let char = text.slice(-1);
    if (char === '<') tag = true;
    if (char === '>') tag = false;

    if (tag) return autoType();
        setTimeout(autoType, 25);

    if ((i + 1) === str.length) {
        btn.className = "show";
        instructions.classList.remove("hide");
        rmaudio.classList.remove("hide");
    }
}
autoType();
let br1 = document.getElementById("br1");
let br2 = document.getElementById("br2");
let br3 = document.getElementById("br3");
let br4 = document.getElementById("br4");
let br5 = document.getElementById("br5");

btn.addEventListener("click", function() {
    btn.classList.add("hide");
    btn.classList.remove("show");
    rmaudio.classList.add("hide");
    instructions.classList.add("hide");
    br1.classList.remove("hide");
    br2.classList.remove("hide");
    br3.classList.remove("hide");
    br4.classList.remove("hide");
    br5.classList.remove("hide");
    setTimeout(function() {
        selection = prompt("Which horse would you like to bet on? Miss Persia, Rusty, The Jet, Germaine, Costco (case-sensitive)");
        game.classList.add("show");
        game.classList.remove("hide");
        start.classList.add("show");
        start.classList.remove("hide");
        chosenHorse.innerHTML = "You have chosen " + selection + "! Press start race to begin the derby!";
    }, delay);
});

rmaudio.addEventListener("click", function() {
    startSound.setAttribute("src", null);
    audience.setAttribute("src", null);
    gallop.setAttribute("src", null);
    rmaudio.classList.add("hide");
});

const andoff = document.getElementById("andoff");

gameover.addEventListener("click", function() {
    btn.classList.add("hide");
    btn.classList.remove("show");
    gameover.classList.add("hide");
    gameover.classList.remove("show");
    rmaudio.classList.add("hide");
    instructions.classList.add("hide");
    br1.classList.remove("hide");
    br2.classList.remove("hide");
    br3.classList.remove("hide");
    br4.classList.remove("hide");
    br5.classList.remove("hide");
    winner.classList.add("hide");

    setTimeout(function() {
        selection = prompt("Which horse would you like to bet on? Miss Persia, Rusty, The Jet, Germaine, Costco (case-sensitive)");
        game.classList.add("show");
        game.classList.remove("hide");
        start.classList.add("show");
        start.classList.remove("hide");
        chosenHorse.innerHTML = "You have chosen " + selection + "! Press start race to begin the derby!";
    }, delay);
});

start.addEventListener("click", function() {
        start.classList.add("hide");
        start.classList.remove("show");
        andoff.classList.remove("hide");
        chosenHorse.classList.add("hide");
        andoff.innerHTML = "And they're off!";
        startSound.play();
        audience.play();
        gallop.play();
        function horseRace(obj, start, end) {
            if(start >= end) {
                
                return;
            }
        else {
            let cont = obj;
            cont.style.marginLeft = start + "px";
            setTimeout(function() {
                horseRace(obj, start + 1, end);
            }, Math.floor(Math.random() * 40));
        }
    }

    function startRace() {
        horseRace(missPersia, -825, 1026);
        horseRace(rusty, -825, 1026);
        horseRace(costco, -825, 1026);
        horseRace(germaine, -825, 1026);
        horseRace(theJet, -825, 1026);
    }
    startRace();
   const tm = setTimeout(function() {
    const intv = setInterval(function finish(winwin) {
        console.log("testingworkingok");
                if (window.getComputedStyle(missPersia, null).getPropertyValue("margin-left") === window.getComputedStyle(finishLine, null).getPropertyValue("margin-left")) {
                console.log("success");
                winner.innerHTML = "Miss Persia is the winner!";
                andoff.innerHTML = "FINISH!"
                winnersList.push("Miss Persia");
                clearTimeout(tm);
                clearInterval(intv);
                gameover.classList.remove("hide");
                if (missMargin === finishLineMargin && selection == "Miss Persia") {
                    winwin = missPersia
                }
                return;
            } else if (window.getComputedStyle(rusty, null).getPropertyValue("margin-left") === window.getComputedStyle(finishLine, null).getPropertyValue("margin-left")) {
                console.log("success");
                winner.innerHTML = "Rusty is the winner!";
                andoff.innerHTML = "FINISH!"
                winnersList.push("Rusty");
                clearTimeout(tm);
                clearInterval(intv);
                gameover.classList.remove("hide");
                if (rustyMargin === finishLineMargin && selection == "Rusty") {
                    winwin = rusty
                }
                return;
            } else if (window.getComputedStyle(costco, null).getPropertyValue("margin-left") === window.getComputedStyle(finishLine, null).getPropertyValue("margin-left")) {
                console.log("success");
                winner.innerHTML = "Costco is the winner!";
                andoff.innerHTML = "FINISH!"
                winnersList.push("Costco");
                clearTimeout(tm);
                clearInterval(intv);
                gameover.classList.remove("hide"); 
                if (costcoMargin === finishLineMargin && selection == "Costco") {
                    winwin = costco
                }
                return;
            } else if (window.getComputedStyle(germaine, null).getPropertyValue("margin-left") === window.getComputedStyle(finishLine, null).getPropertyValue("margin-left")) {
                console.log("success");
                winner.innerHTML = "Germaine is the winner!";
                andoff.innerHTML = "FINISH!"
                winnersList.push("Germaine");
                clearTimeout(tm);
                clearInterval(intv);
                gameover.classList.remove("hide"); 
                if (germaineMargin === finishLineMargin && selection == "Germaine") {
                    winwin = germaine
                }
                return;
            } else if (window.getComputedStyle(theJet, null).getPropertyValue("margin-left") === window.getComputedStyle(finishLine, null).getPropertyValue("margin-left")) {
                console.log("success");
                winner.innerHTML = "The Jet is the winner!";
                andoff.innerHTML = "FINISH!"
                winnersList.push("The Jet");
                clearTimeout(tm);
                clearInterval(intv); 
                gameover.classList.remove("hide");
                if (theJetMargin === finishLineMargin && selection == "The Jet") {
                    winwin = theJet
                }
                return;
            }
        }, 75)}, 10000);
        // function winnerwinner() {
        //     for (i = 0; i < winnersList.length; i++) {
        //         winnersText.innerHTML = winnersList[i];
        //     }
        // }
        // winnerwinner();
});

    